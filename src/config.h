#ifndef config_h_
#define config_h_

/***************************************************************************

   My Espressif ESP-8266 program to control chip and make some IoT stuff.

 ****************************************************************************/

/******************** CONFIGURE USER OPTIONS ************************************/

/*** Defining software options ***/

#define BAUDRATE 115200
#define MAX_SENSORS 5

/******* TIME ******************/

#define TIME_READ_DELAY 0 //For sensor read delay.
#define SENDING_DELAY  1000*60*10 //how often MQTT will send mesage in mls.

/******** WIFI ***********/

#define NETWORKSSID "**********"   //your network SSID
#define NETWORKPSWD "**********" //Your netowrk Password

/******************* MQTT *********************/

#define MQTT_SERVER "*****" //IP Adress of MQTT Broker
#define MQTT_PORT 1883            //Port of broker - normally 1883
#define CLIENTID "********"      //Name of device in MQTT

#define TOPIC "**********"

#define TOPIC_TEMP "*************" // Prefered topic of Temperature state
#define TOPIC_HUM "******************" // Prefered topic of humidity state
#define TOPIC_PRES "*****************" // Prefered topic of humidity state


/************* DEFINING HRDWARE PIN CONGIFURATION *************/

#define I2C_SDA_PIN 2
#define I2C_SCL_PIN 4

#endif
