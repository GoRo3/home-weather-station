#include "ntw_general.h"

WiFiClient espClient;
PubSubClient client(espClient);

/*****************************************************************************
   *                        Constructors
   * **************************************************************************/

//  for Mqtt

ntw_general::ntw_general(String _clientid_, String _mqtt_server_, uint16_t _mqtt_port_) {
  _clientid = _clientid_;
  _mqtt_server=_mqtt_server_;
  _mqtt_port=_mqtt_port_;
  }

/*****************************************************************************
   *                        WIFI
   * **************************************************************************/

void ntw_general::WiFiConnect(String ssid, String pswrd)
{
    WiFi.begin(ssid.c_str(), pswrd.c_str());
    delay(500);

    Serial.print("Connecting to network: ");
    Serial.println(ssid);

    while (WiFi.status() != WL_CONNECTED)
    {
        uint8_t i=0;
        Serial.print("...");
        delay(1000*5);
        i++;
        if (i == 5) ESP.restart();
        
    }
    Serial.print("Connected to network. ");
    Serial.print("Lokal IP number: ");
    Serial.println(WiFi.localIP());
}

/*****************************************************************************
   *                        MQTT
   * **************************************************************************/

void ntw_general::mqtt_set_server()
{
    client.setServer(MQTT_SERVER, MQTT_PORT);
}

void ntw_general::sendMqttMsg(String Msg, String Topic)
{
    Serial.print("Message: ");
    Serial.print(Msg);
    Serial.print(" On topic: ");
    Serial.println(Topic);

    boolean connected = client.connected();
    if (!connected)
    {
        connect_mqtt();
    }
    client.publish(Topic.c_str(), Msg.c_str(), false);
    Serial.print(" published: ");
    Serial.println(Msg);
}

void ntw_general::connect_mqtt()
{
    Serial.println("Connecting to MQTT Server. ");
    delay(50);

    if (client.connect(CLIENTID)) //Connecting to MQTT brocker.
    {
        Serial.print("Connected to: ");
        Serial.println(_mqtt_server);
        Serial.print("As: ");
        Serial.println(CLIENTID);
    }
    else
    { //What if not connected - debug.

        switch (client.state())
        {
        case -4:
            Serial.println("MQTT_CONNECTION_TIMEOUT - the server didn't respond within the keepalive time");
            break;
        case -3:
            Serial.println("MQTT_CONNECTION_LOST - the network connection was broken");
            break;
        case -2:
            Serial.println("MQTT_CONNECT_FAILED - the network connection failed");
            break;
        case -1:
            Serial.println("MQTT_DISCONNECTED - the client is disconnected cleanly");
            break;
        case 1:
            Serial.println("MQTT_CONNECT_BAD_PROTOCOL - the server doesn't support the requested version of MQTT");
            break;
        case 2:
            Serial.println("MQTT_CONNECT_BAD_CLIENT_ID - the server rejected the client identifier");
            break;
        case 3:
            Serial.println("MQTT_CONNECT_UNAVAILABLE - the server was unable to accept the connection");
            break;
        case 4:
            Serial.println("MQTT_CONNECT_BAD_CREDENTIALS - the username/password were rejected");
            break;
        case 5:
            Serial.println("MQTT_CONNECT_UNAUTHORIZED - the client was not authorized to connect");
            break;
        default:
            Serial.println("MQTT_CONNECTED - the cient is connected");
            break;
        }
    }
}

void ntw_general::handleMqtt()
{
    client.loop();
}
