/***************************************************************************

 My Espressif ESP-8266 program to control chip and make some IoT stuff.

 ****************************************************************************/

/******************** CONFIGURE USER OPTIONS ************************************/

#include <Arduino.h>
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_BMP280.h>
#include <AM2320.h>

#include "ntw_general.h"
#include "config.h"

/*************************** GLOBAL VARIABLES *******************************/

uint baudrate = BAUDRATE;

String networkssid = NETWORKSSID;
String networkpaswd = NETWORKPSWD;
String clientid = CLIENTID;
String mqtt_server = MQTT_SERVER;
uint16_t mqtt_port = MQTT_PORT;
unsigned long time = 0;
unsigned long time_interval = SENDING_DELAY; 

struct sensor
{
    float temperature = 0, humidity = 0, pressure = 0;
    String sensor_name = CLIENTID;
    byte use_flag = 0;
};

struct Flags
{
    int use_mqtt : 1;
} flags;

/************************** FUNCION DECLARATION ****************************/

void PinSetup();
void read_i2c_sensors();
void add_sensor();
void init_sensor();
void send_i2c_sensor();

/************************ MAIN PROGRAM FUNCION **************************/

Adafruit_BMP280 BMP;
AM2320_Sensor AMS;
ntw_general ntw(CLIENTID, MQTT_SERVER, MQTT_PORT);

sensor *ptr_data = new sensor[MAX_SENSORS];

void setup()
{
    Serial.begin(baudrate);
    delay(1000);
    Serial.println("H!");


    ntw.WiFiConnect(networkssid, networkpaswd); // Connecting to WiFi

    Wire.begin(I2C_SDA_PIN, I2C_SCL_PIN); //Begin i2c communication
    if (!BMP.begin(0x76))                 //Adres sensora BM180 0x76.
    {
        Serial.println("Could not find a valid BMP280 sensor, check wiring!");
    }

    init_sensor(); //Initializing on board sensor.

        //MQTT init
     
    ntw.mqtt_set_server();
    ntw.connect_mqtt();
  
}

void loop()
{
    unsigned long current_time=millis();

    ntw.handleMqtt();

    if (current_time - time > time_interval)
    {
        time = current_time;

        read_i2c_sensors();
        send_i2c_sensor();
        
    }
}

void read_i2c_sensors()
{
    for (int i = 0; i < MAX_SENSORS; i++)
    {
        if (ptr_data[i].use_flag)
        {
            ptr_data[i].temperature = AMS.read_temperature();
            ptr_data[i].humidity = AMS.read_humidity();
            ptr_data[i].pressure = BMP.readPressure() / 100;

            Serial.print("sensor_name: ");
            Serial.print(ptr_data[i].sensor_name);
            Serial.print(", ");
            Serial.print("Pressure: ");
            Serial.print(ptr_data[i].pressure);
            Serial.print(", ");
            Serial.print("Temperature: ");
            Serial.print(ptr_data[i].temperature);
            Serial.print(", ");
            Serial.print("Humidity: ");
            Serial.print(ptr_data[i].humidity);
            Serial.print("\n");
        }
    }
}

void send_i2c_sensor()
{
        ntw.sendMqttMsg(String(ptr_data->temperature), TOPIC_TEMP);
        ntw.sendMqttMsg(String(ptr_data->humidity), TOPIC_HUM);
        ntw.sendMqttMsg(String(ptr_data->pressure), TOPIC_PRES);
}

void init_sensor()
{
    ptr_data[0].sensor_name = CLIENTID;
    ptr_data[0].use_flag = 1;
}