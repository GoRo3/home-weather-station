#ifndef ntw_general_h_
#define ntw_general_h_

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include "config.h"


class ntw_general
{
public:
/*****************************************************************************
   *                        WIFI
   * **************************************************************************/

  ntw_general(); 
  ntw_general(String _clientid_, String _mqtt_server_, uint16_t _mqtt_port_);

/*****************************************************************************
   *                        WIFI
   * **************************************************************************/

  void WiFiConnect(String ssid, String pswd);

  /*****************************************************************************
   *                        MQTT
   * **************************************************************************/
  void connect_mqtt();
  
  void MQTT_msg_Sensor();
  void sendMqttMsg(String Msg, String Topic);  
  void handleMqtt();
  void mqtt_set_server();

private:
  String _clientid;
  String _mqtt_server;
  uint16_t _mqtt_port;

};
#endif